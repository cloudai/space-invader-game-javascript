/*************************
 ** Space Invader Game **
 *************************/

/**
 * DATA STRUCTURES
 */

function point(x,y) {
    this.x = x;
    this.y = y;
}

/* new point(x,y) represents a coordinate of a point on a plane
 */

var ufo = function(x,y,d) {
    
    this.base = point;
    this.base(x,y);
    
    this.color = "green";
    this.radius = 6;
    this.wingspan = 10;
    this.wing_width = this.wingspan*0.2;
    this.diameter = this.radius*2;
    this.x_vel = 5;
    this.y_vel = 2;
    this.direction = d || "left";
};

/* A UFO is a new ufo(x,y); ufo is a substructure of point.
* interp. new ufo(x,y) is the UFO's current location
*/

var tank = function(x) {
    this.x = x;
    this.velocity = 4;
    this.height = 10;
    this.width = 20;
    this.color = "brown";
    this.x_gun = this.x + (this.width/2);
    this.gun_width = this.width*0.2;
    this.gun_height = 7;
    this.fire_height = this.height + this.gun_height;
};

tank.prototype = {
  // @param: string, either 'right of 'left
  move: function(direction) {
    if(direction=="right") {
      return this.x+this.velocity;
    } else if(direction=="left") {
      return this.x-this.velocity;
    }     
  },
};

/* A Tank is new tank(Number,Number).
* interp. new tank(x,dx) means the tank is at (x ,HEIGHT)
*   and that it moves dx pixels per clock tick; 
*/

function missile(x,y) {

    this.base = point;
    this.base(x,y);
    
    this.color = "red";
    this.radius = 3;
    this.velocity = 4;
}

/* A Missile is new missile(x,y); missile is a substructure of point.
* interp. new missile(x,y) is the missile's current location
*/

/* MissileStack is an array of missiles, launched by a tank
*/

var sigs = function(ufo,tank,missile_stack,gs) {
    this.ufo = ufo;
    this.tank = tank;
    this.missile_stack = missile_stack;
    this.game_status = gs || "in-progress";
};

/* A SIGS (short for “space invader game state”) 
*    is new sigs(ufo,tank,missile_stack,game_status)
* game status property is either: 'in-progress', 'paused', 'won', 'lost'
* interp. represents a state of space invader game
*/

/**
 * CONSTANTS
 */

// canvas measures
const WIDTH = 300;
const HEIGHT = 400;
const m_board = document.getElementById("board");

/**
 * FUNCTIONS
 */

/* SIGS -> Image-on-html5-canvas
 * consume an instance of sigs and render the game objects
 */
function to_image(g) {
    var canvas = document.getElementById("scene");
    canvas.width = WIDTH;
    canvas.height = HEIGHT;
    var ctx = canvas.getContext('2d');
    if(g.game_status=="won") {
        draw_text(ctx,"You won!");
        m_board.innerText = "Game Over, nice result!";
    } else if(g.game_status=="lost") {
        draw_text(ctx,"You lost!");
        m_board.innerText = "Hit start and try again!";
    } else if(g.game_status=="in-progress") {
        m_board.innerText = "";
        draw_tank(ctx,g);
        draw_ufo(ctx,g);
        draw_missiles2(ctx,g);
    }
}

function draw_text(ctx,message) {
    ctx.fillStyle = "red";
    ctx.font = "24pt Helvetica";
    ctx.fillText(message,WIDTH/4,HEIGHT/2);
}

function draw_tank(ctx,g) {
    ctx.fillStyle = g.tank.color;
    ctx.fillRect(g.tank.x,HEIGHT-g.tank.height,g.tank.width,g.tank.height);
    ctx.beginPath();
    ctx.moveTo(g.tank.x_gun,HEIGHT-g.tank.height);
    ctx.lineTo(g.tank.x_gun,HEIGHT-g.tank.height-g.tank.gun_height);
    ctx.lineWidth = g.tank.gun_width;
    ctx.strokeStyle = g.tank.color;
    ctx.stroke();
    ctx.closePath();
}

function draw_ufo(ctx,g) {
  ctx.fillStyle = g.ufo.color;
  ctx.arc(g.ufo.x,g.ufo.y,g.ufo.radius,0,Math.PI*2);
  ctx.fill();
  ctx.beginPath();
  ctx.moveTo(g.ufo.x-g.ufo.wingspan,g.ufo.y);
  ctx.lineTo(g.ufo.x+g.ufo.wingspan,g.ufo.y);
  ctx.lineWidth = g.ufo.wing_width;
  ctx.strokeStyle = g.ufo.color;
  ctx.stroke();
  ctx.closePath();
}
// @params: CanvasRenderingContext2D Array<Missile>
function draw_missile(ctx,missile) {
  ctx.fillStyle = missile.color;
  ctx.arc(
      missile.x,missile.y,missile.radius,0,Math.PI*2);
  ctx.fill();
} 
// @params: CanvasRenderingContext2D SIGS
function draw_missiles(ctx,game) {
  for(var i=0;i<game.missile_stack.length;i++) {
      draw_missile(ctx,game.missile_stack[i]);
  }
}

function draw_missiles2(ctx,game) {
    /** Context Missile -> Context
     *  draw a missile on a canvas
     */
    function callback(c,m) {
        draw_missile(c,m);
        return c;
    }
    /** (Context Missile -> Context) Context -> Context
     *  traverse array of missiles to draw them on a canvas;
     *  supply rendering context as an initial value
     */
    game.missile_stack.reduce(callback,ctx);
}

/* KeyEvent SIGS -> SIGS
 * modify sigs instance according to key events
 */
function _on_key(ke,s) {
    if((ke.keyCode==39)&&(s.tank.x<(WIDTH-s.tank.width))) {
      return new sigs(s.ufo,new tank(s.tank.move("right")),s.missile_stack);
    
    } else if((ke.keyCode==37)&&(s.tank.x>=0)) {
      return new sigs(s.ufo,new tank(s.tank.move("left")),s.missile_stack);
    
    } else if(ke.keyCode==70) {
      return new sigs(
          s.ufo,s.tank,
          push_and_return(
              s.missile_stack,new missile(s.tank.x_gun,HEIGHT-s.tank.fire_height)));
    } else {
      return s;
    }
}

/* Array -> Array
 * push a value in a given array and return array
 */
 function push_and_return(array,value) {
    array.push(value);
    return array;
 }

/* SIGS -> SIGS
 * modify sigs game in response to tick events
 *
 * Note: distinguish between landed ufo and launched missiles
 */
function tick(s) {
    if(s.ufo.y < (HEIGHT - s.ufo.radius)) {
        if((missile_hit_ufo_v2(s.missile_stack,s.ufo,s.ufo.diameter*0.8))) {
            return ufo_hit(s);
        } else {
            return game_in_progress(s);
        }
    } else {
        return ufo_landed(s);
    }
}

/* SIGS -> SIGS
 * create a world with firing missiles and descending ufo
 */
function game_in_progress(s) {
    return new sigs(
        ufo_to_direction(switch_ufo_direction(s.ufo)),
        s.tank,
        propel_missiles(
            remove_used_missiles(s.missile_stack)));
}

/* Ufo String -> Ufo
 * produce a ufo that flies in a specified direction;
 * note: save current direction by providing direction property
 */
function ufo_to_direction(u) {
    if(u.direction=="right") {
        return new ufo(u.x+u.x_vel,u.y+u.y_vel,u.direction);
    } else if(u.direction=="left") {
        return new ufo(u.x-u.x_vel,u.y+u.y_vel,u.direction);
    }
}

/* Ufo -> Ufo
 * switch ufo direction randomly and if it hit left or right border
 */
function switch_ufo_direction(u) {
    if((u.x<=5)&&(u.direction=="left")) {
        return new ufo(u.x,u.y,"right");
    } else if((u.x>=WIDTH-5)&&(u.direction=="right")) {
        return new ufo(u.x,u.y,"left");
    } else {
        return switch_ufo_direction_randomly(u);
    }
}

/* Ufo -> Ufo
 * switch ufo direction randomly
 */
function switch_ufo_direction_randomly(u) {
    if(Math.floor(Math.random()*8)==2) {
        if(u.direction=="right") {
            return new ufo(u.x,u.y,"left");
        } else {
            return new ufo(u.x,u.y,"right");
        }
    } else {
        return u;
    }    
}

/* SIGS -> SIGS
 * create a world state where a ufo is hit by a tank
 */
function ufo_hit(s) {
    return new sigs(
        s.ufo,s.tank,s.missile_stack,"won");
}

/* SIGS -> SIGS
 * create a world state where a ufo landed successfully 
 */
function ufo_landed(s) {
    return new sigs(
        s.ufo,s.tank,s.missile_stack,"lost");
}

/* Missile -> Missile
 * move missile up a scene
 */
function propel_missile(m) {
    return new missile(m.x,m.y-m.velocity);
} 

/* Array<Missile> -> Array<Missile>
 * move a stack of missiles up a scene
 */
function propel_missiles(lom) {
    if(lom.length===0) {
        return [];
    } else {
        return [propel_missile(lom[0])].concat(
            propel_missiles(lom.slice(1,lom.length)));
    }
}

/* Point Point -> Number
 * measure distance between two points on a plane
 */
function calc_distance(a,b) {
    return Math.sqrt(
        Math.pow(a.x - b.x,2) + Math.pow(a.y - b.y,2));
}

/* Array<Missiles> Ufo Number -> Boolean
 * check if every missile in a stack is at distance defined by number from a ufo
 */
function missile_hit_ufo(lom,u,n) {
    if(lom.length===0) {
        return false;
    } else {
        if(calc_distance(lom[0],u) < n) {
            return true;
        } else {
            missile_hit_ufo(lom.slice(1,lom.length),u,n);
        }
    }
}

/* Array<Missiles> Ufo Number -> Boolean
 * check if every missile in a stack is at distance defined by number from a ufo
 */
function missile_hit_ufo_v2(lom,u,n) {
    /** Missile -> Boolean
     * check if missile is within distance n to ufo
     */
    function close_enough(m) {
        return calc_distance(m,u) <= n;
    }
    return lom.some(close_enough);
}


/* Array<Missile> -> Array<Missile>
 * remove missiles that have flown beyond the limits of a scene
 */
function remove_used_missiles(lom) {
    if(lom.length===0) {
        return [];
    } else if(lom[0].y<=10) {
        return remove_used_missiles(lom.slice(1,lom.length));
    } else {
        return remove_used_missiles(lom.slice(1,lom.length)).concat(lom[0]);
    }
}  

// Structure for running the game
function BigBang(g) {
 // holds current state of the game world
 this.game = g;
}

// interactive functions that operate on a current world state
BigBang.prototype = {
    to_draw: function(to_image) {
        var bigbang = this;
        var callback = function(t) {
            to_image(bigbang.game);
            bigbang.to_draw(to_image);
        };
        window.requestAnimationFrame(callback);
        return this;
    },
    on_tick: function(tick,ms) {
        var bigbang = this;
        function callback() {
          bigbang.set_game(tick(bigbang.game));
        }
        window.setInterval(callback,ms);
        return this;
    },
    on_key: function(_on_key) {
        var bigbang = this;
        // modify the game world in response to key events
        function callback(ke) {
          bigbang.set_game(_on_key(ke,bigbang.game));
        }
        // register key event handler
        document.addEventListener("keydown",callback);
        return this;
    },
    set_game: function(g) {
        this.game = g;
    }
};

/* BigBang structure is responsible for launching the game.
 */

/* Start the game
 */

function main() {
    function game() {
        (new BigBang(
            new sigs(new ufo(Math.random()*(WIDTH-10),50),new tank(10),[])))
            .to_draw(to_image)
            .on_key(_on_key)
            .on_tick(tick,50);
    }
    document.getElementById("start").addEventListener("click",game);
}









